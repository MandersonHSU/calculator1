package com.example.michaelanderson.guicalculater;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button _9, _8, _7, _6, _5, _4, _3, _2, _1, _0, btnAdd, btnMinus, btnDivid, btnMultiply, btnExpo,
            btnSqr, btnBrackt, btnPrin, btnDot, btnCos, btnSin, btnTan, btnClear, btnSum;

    EditText resultsTextView;
    float firstVal, secVal, answer;;

    Boolean isAdd, isMin, isMulti, isDiv, isExp, isSqr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _9 = (Button) findViewById(R.id.btn9);
        _8 = (Button) findViewById(R.id.btn8);
        _7 = (Button) findViewById(R.id.btn7);
        _6 = (Button) findViewById(R.id.btn6);
        _5 = (Button) findViewById(R.id.btn5);
        _4 = (Button) findViewById(R.id.btn4);
        _3 = (Button) findViewById(R.id.btn3);
        _2 = (Button) findViewById(R.id.btn2);
        _1 = (Button) findViewById(R.id.btn1);
        _0 = (Button) findViewById(R.id.btn0);

        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnMinus = (Button) findViewById(R.id.btnMinus);
        btnDivid = (Button) findViewById(R.id.btnDivid);
        btnMultiply = (Button) findViewById(R.id.btnMultiply);

        btnExpo = (Button) findViewById(R.id.btnExpo);
        btnSqr = (Button) findViewById(R.id.btnSqr);

        btnBrackt = (Button) findViewById(R.id.btnBrackt);
        btnPrin = (Button) findViewById(R.id.btnPrin);
        btnDot = (Button) findViewById(R.id.btnDot);

        btnCos = (Button) findViewById(R.id.btnCos);
        btnSin = (Button) findViewById(R.id.btnSin);
        btnTan = (Button) findViewById(R.id.btnTan);

        btnSum = (Button) findViewById(R.id.btnEquals);
        btnClear = (Button) findViewById(R.id.btnClear);

        resultsTextView = (EditText) findViewById(R.id.resultTextView);
        resultsTextView.setText("");


        _9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {resultsTextView.append("9");}
        });
        _8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {resultsTextView.append("8");}
        });
        _7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {resultsTextView.append("7");}
        });
        _6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {resultsTextView.append("6");}
        });
        _5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {resultsTextView.append("5");}
        });
        _4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {resultsTextView.append("4");}
        });
        _3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {resultsTextView.append("3");}
        });
        _2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {resultsTextView.append("2");}
        });
        _1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {resultsTextView.append("1");}
        });
        _0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {resultsTextView.append("0");}
        });
        btnDot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {resultsTextView.append(".");}
        });


        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (resultsTextView.getText().toString().equalsIgnoreCase("")) {
                    resultsTextView.setText("");
                } else {
                    firstVal = Float.parseFloat(resultsTextView.getText() + "");
                    resultsTextView.setText(null);
                    isAdd = true;
                }
            }
        });


        btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firstVal = Float.parseFloat(resultsTextView.getText()+"");
                isMin = true;
                resultsTextView.setText(null);
            }
        });

        btnMultiply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {resultsTextView.append("9");}
        });

        btnDivid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {resultsTextView.append("9");}
        });

        btnExpo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {resultsTextView.append("9");}
        });

        btnSqr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {resultsTextView.append("9");}
        });

        btnSin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {resultsTextView.append("9");}
        });

        btnCos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {resultsTextView.append("9");}
        });

        btnTan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {resultsTextView.append("9");}
        });

        btnPrin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {resultsTextView.append("9");}
        });

        btnBrackt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {resultsTextView.append("9");}
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {resultsTextView.append("9");}
        });

        btnSum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                secVal = Float.parseFloat(resultsTextView.getText()+"");
                resultsTextView.setText("");
                if(isAdd){
                    answer = firstVal + secVal;
                    resultsTextView.append(answer + "");
                    isAdd = false;
                }
                if(isMin){
                    answer = firstVal + secVal;
                    resultsTextView.append(answer + "");
                    isMin = false;
                }
            }
        });

    }//onCreat
}//main
